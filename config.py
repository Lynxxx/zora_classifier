import os
BASE_DIR = os.path.abspath(os.path.dirname(__file__))

DEBUG = True

SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(BASE_DIR, 'database.db')
SQLALCHEMY_TRACK_MODIFICATIONS = False

# Settings default values
DEFAULT_ZORA_PULL_INTERVAL = 1                          # days
DEFAULT_RESOURCE_TYPE_UPDATE_INTERVAL = 14              # days
DEFAULT_INSTITUTE_UPDATE_INTERVAL = 14                  # days
DEFAULT_ZORA_URL = 'https://www.zora.uzh.ch/cgi/oai2'
DEFAULT_ANNOTATION_TIMEOUT = 30                         # minutes

# Machine Learning Tool
LEGACY_ANNOTATIONS_PATH = os.path.join(BASE_DIR, 'zora_classifier/static/legacy_annotations/legacy_annotations.json')
ML_MODEL_FOLDER = os.path.join(BASE_DIR, 'zora_classifier/ml_model')

# Babel
LANGUAGES = {
    'en': 'English',
    'de': 'German'
}
BABEL_DEFAULT_LOCALE = 'en'

SECRET_KEY = os.urandom(32)
