import os
import threading
import csv
import locale
import zipfile

from flask import redirect, url_for, render_template, request, session, make_response, jsonify, abort
from flask_babel import gettext
from flask_login import current_user, login_user, logout_user
from werkzeug.utils import secure_filename

from zora_classifier import db, server_app, server_logic_instance
from zora_classifier.models import Creator, DDC, Institute, Keyword, Paper, ServerSetting, User, Language, Top10Statistic, \
    GeneralStatistic, GraphStatistic
from zora_classifier.utils import login_required
from sqlalchemy import extract, exists, or_, and_, func
from io import StringIO
from werkzeug.wrappers import Response

DEFAULT_FILTERS = {
    'title_filter': '',
    'creator_filter': '',
    'keyword_filter': '',
    'language_filter': '',
    'date_min_filter': '',
    'date_max_filter': '',
    'ddc_filter': '',
    'order_by': 'date desc',
    'export': False
}


# ----------------- ROUTES -----------------------
# The entry point of the application that is called from magnolia. We set the language here to be able to use the
# magnolia language selection.
@server_app.route('/', methods=['GET'])
def start():
    lang = request.args.get('lang')
    session['lang'] = lang
    if lang == 'de':
        locale.setlocale(locale.LC_ALL, 'de_CH.utf8')
    else:
        locale.setlocale(locale.LC_ALL, 'en_GB.utf8')
    return redirect(url_for('search'))


# Basic error handling. The given message will be displayed on the error page.
@server_app.errorhandler(401)
@server_app.errorhandler(404)
@server_app.errorhandler(408)
def custom_error_handler(error):
    message = gettext('GENERAL_ERROR_MESSAGE')
    if error and error.code == 401:
        message = gettext('401_ERROR_MESSAGE')
    if error and error.code == 404:
        message = gettext('404_ERROR_MESSAGE')
    if error and error.code == 408:
        message = gettext('408_ERROR_MESSAGE')
    link = url_for('search')
    return render_template('error.html', message=message, link=link, user=current_user)


# Internal server error handling. The error message will be displayed on the current page.
@server_app.errorhandler(500)
def custom_internal_error_handler(error):
    data = {'error': str(error.description)}
    return make_response(jsonify(data), 500)


# The main page of the application where a user can search for positively classified papers.
@server_app.route('/search', defaults={'page': 1}, methods=['GET'])
@server_app.route('/search/<int:page>', methods=['GET'])
def search(page):

    # Get all positively classified papers
    matching_papers = db.session.query(Paper).filter(Paper.classification == True)

    # Get dropdown values
    languages = db.session.query(Language).join(Paper).filter(Paper.classification == True).order_by(Language.name).distinct().all()
    ddcs = db.session.query(DDC).join(Paper.ddcs).filter(Paper.classification == True).order_by(DDC.dewey_number).distinct().all()

    # Set default filters if no arguments are passed
    if request.args:
        filters = request.args
    else:
        filters = DEFAULT_FILTERS

    # Apply filters
    for filter, value in filters.items():
        if not value or len(value) == 0:
            continue

        # Title filter
        if filter == 'title_filter':
            values = value.split(' ')
            for filter_value in values:
                matching_papers = matching_papers.filter(func.lower(Paper.title).contains(func.lower(filter_value)))

        # Author filter
        # We need to join with the Creator table to be able to search for papers with a specific author. Because of
        # this, we also need to call .distinct().
        if filter == 'creator_filter':
            creator_list = value.split(' ')
            matching_papers = matching_papers.join(Creator, Paper.creators)
            if len(creator_list) == 1:
                matching_papers = matching_papers.filter(or_(func.lower(Creator.last_name).contains(func.lower(creator_list[0])),
                                                             func.lower(Creator.first_name).contains(func.lower(creator_list[0]))))
            else:
                matching_papers = matching_papers.filter(or_(and_(func.lower(Creator.last_name).contains(func.lower(creator_list[0])),
                                                                  func.lower(Creator.first_name).contains(func.lower(creator_list[1]))),
                                                             and_(func.lower(Creator.last_name).contains(func.lower(creator_list[1])),
                                                                  func.lower(Creator.first_name).contains(func.lower(creator_list[0])))))
            matching_papers = matching_papers.distinct(Paper.uid)

        # Keyword filter
        # We need to join with the Keyword table to be able to search for papers with a specific keyword. Because of
        # this, we also need to call .distinct().
        if filter == 'keyword_filter':
            matching_papers = matching_papers.join(Keyword, Paper.keywords)
            matching_papers = matching_papers.filter(func.lower(Keyword.name).contains(func.lower(value)))
            matching_papers = matching_papers.distinct(Paper.uid)

        # Language filter
        if filter == 'language_filter':
            matching_papers = matching_papers.filter(Paper.language_id == value)

        # DDC filter
        # We need to join with the DDC table to be able to search for papers with a specific ddc. Because of
        # this, we also need to call .distinct().
        if filter == 'ddc_filter':
            matching_papers = matching_papers.join(DDC, Paper.ddcs)
            matching_papers = matching_papers.filter(DDC.dewey_number == value).distinct(Paper.uid)
            matching_papers = matching_papers.distinct(Paper.uid)

        # Min date filter
        if filter == 'date_min_filter':
            matching_papers = matching_papers.filter(extract('year', Paper.date) >= int(value))

        # Max date filter
        if filter == 'date_max_filter':
            matching_papers = matching_papers.filter(extract('year', Paper.date) <= int(value))

        # Sort
        if filter == 'order_by':
            order_by = value.split(' ')[0]
            order = value.split(' ')[1]
            if order_by == 'title':
                if order == 'asc':
                    matching_papers = matching_papers.order_by(Paper.title.asc())
                elif order == 'desc':
                    matching_papers = matching_papers.order_by(Paper.title.desc())
            elif order_by == 'date':
                if order == 'asc':
                    matching_papers = matching_papers.order_by(Paper.date.asc())
                elif order == 'desc':
                    matching_papers = matching_papers.order_by(Paper.date.desc())

    # Pagination
    paginated = matching_papers.paginate(page)

    # Export function
    if request.args.get('type') == 'export':
        def generate_csv():
            data = StringIO()
            w = csv.writer(data, dialect='excel')
            header = (gettext('TITLE'), gettext('AUTHOR'), gettext('YEAR'), gettext('LANGUAGE'))
            w.writerow(header)
            yield data.getvalue()
            data.seek(0)
            data.truncate(0)
            for paper in matching_papers:
                creator_string = ''
                first = True
                for creator in paper.creators:
                    if first:
                        creator_string = creator.first_name + ' ' + creator.last_name
                        first = False
                    else:
                        if creator is not None:
                            if creator.first_name is not None:
                                creator_string = creator_string + ', ' + creator.first_name + ' ' + creator.last_name
                if paper.language is not None:
                    language = paper.language.name
                row = (paper.title, creator_string, paper.date.year, language)
                w.writerow(row)
                yield data.getvalue()
                data.seek(0)
                data.truncate(0)

        response = Response(generate_csv(), mimetype="text/csv")
        response.headers.set("Content-Disposition", "attachment", filename="export.csv")
        return response
    else:
        return render_template('search.html', paginated=paginated, user=current_user, filters=filters, ddcs=ddcs,
                               languages=languages, page=page)


# The server settings page, where admins can change server settings or redeploy the machine learning model.
@server_app.route('/server_settings', methods=['GET'])
@login_required(required_role='admin')
def server_settings():
    server_setting_list = db.session.query(ServerSetting).order_by(ServerSetting.name.asc()).all()
    return render_template('server_settings.html', user=current_user, server_settings=server_setting_list)


@server_app.route('/server_settings', methods=['POST'])
@login_required(required_role='admin')
def change_server_settings():
    try:
        annotation_timeout = request.form.get('annotation_timeout')
        institute_update_interval = request.form.get('institute_update_interval')
        resource_type_update_interval = request.form.get('resource_type_update_interval')
        zora_pull_interval = request.form.get('zora_pull_interval')
        zora_url = request.form.get('zora_url')
        if annotation_timeout:
            ServerSetting.set(name='annotation_timeout', value=int(annotation_timeout))
        if institute_update_interval:
            ServerSetting.set(name='institute_update_interval', value=int(institute_update_interval))
        if resource_type_update_interval:
            ServerSetting.set(name='resource_type_update_interval', value=int(resource_type_update_interval))
        if zora_pull_interval:
            ServerSetting.set(name='zora_pull_interval', value=int(zora_pull_interval))
        if zora_url:
            ServerSetting.set(name='zora_url', value=zora_url)
        db.session.commit()
        return 'success'
    except Exception as e:
        abort(500, e)


@server_app.route('/server_settings/export_annotated_papers', methods=['GET'])
@login_required(required_role='admin')
def export_annotated_papers():
    annotated_papers = db.session.query(Paper).join(Paper.language).filter(Paper.annotated == True).filter(Paper.description != None).filter(Language.name == 'eng').all()

    def generate_csv():

        # Write header
        data = StringIO()
        w = csv.writer(data, dialect='excel')
        header = ('abstract', 'label')
        w.writerow(header)
        yield data.getvalue()
        data.seek(0)
        data.truncate(0)

        # Write data
        for paper in annotated_papers:
            row = (paper.description, int(paper.classification))
            w.writerow(row)
            yield data.getvalue()
            data.seek(0)
            data.truncate(0)

    response = Response(generate_csv(), mimetype="text/csv")
    response.headers.set("Content-Disposition", "attachment", filename="annotated_papers.csv")
    return response


@server_app.route('/server_settings/upload_trained_model', methods=['POST'])
@login_required(required_role='admin')
def upload_trained_model():
    try:
        trained_model_folder = server_app.config['ML_MODEL_FOLDER']
        trained_model_zip_path = os.path.join(trained_model_folder, 'trained_model.zip')
        trained_model = request.files['trained_model']
        if trained_model:
            trained_model.save(trained_model_zip_path)
        with zipfile.ZipFile(trained_model_zip_path, 'r') as zip_ref:
            zip_ref.extractall(trained_model_folder)

        # Start thread to reclassify all papers
        thread = threading.Thread(target=server_logic_instance.apply_trained_model)
        thread.start()
        return 'success'
    except Exception as e:
        abort(500, e)


@server_app.route('/about', methods=['GET'])
def about():
    return render_template('about.html', user=current_user)


@server_app.route('/statistics', methods=['GET'])
def statistics():
    general_statistics = {
        'positive_papers': GeneralStatistic.get('positive_papers'),
        'total_papers': GeneralStatistic.get('total_papers'),
        'ratio_positive_papers': GeneralStatistic.get('ratio_positive_papers'),
        'annotated_papers': GeneralStatistic.get('annotated_papers'),
        'positive_annotated_papers': GeneralStatistic.get('positive_annotated_papers'),
    }
    top_authors = Top10Statistic.get('authors')
    top_keywords = Top10Statistic.get('keywords')
    top_ddcs = Top10Statistic.get('ddcs')
    top_institutes = Top10Statistic.get('institutes')
    graph = GraphStatistic.get('positive_papers_per_year')
    return render_template('statistics.html', general_statistics=general_statistics, top_keywords=top_keywords,
                           user=current_user, graph=graph, top_authors=top_authors, top_institutes=top_institutes,
                           top_ddcs=top_ddcs)


@server_app.route('/annotate', methods=['GET'])
@login_required(required_role='any')
def annotate():
    paper = server_logic_instance.get_annotation()
    return render_template('annotate.html', paper=paper, user=current_user)


@server_app.route('/annotate', methods=['POST'])
@login_required(required_role='any')
def annotate_paper():
    try:
        uid = request.form.get('uid')
        if request.method == 'POST' and uid:
            if request.form.get('skip'):
                server_logic_instance.skip_annotation(uid=uid)
                status = 200
            else:
                classification = request.form.get('classification')
                if classification is not None:
                    status = server_logic_instance.set_annotation(uid=uid, classification=bool(classification))
        else:
            status = 500

        if status == 200:
            return 'success'
        elif status == 408:
            data = {'error': gettext('ERROR_ANNOTATION_TIMEOUT')}
            return make_response(jsonify(data), 408)
        else:
            abort(500, gettext('ERROR_ANNOTATION_FAILED'))
    except Exception as e:
        abort(500, e)


@server_app.route('/user_management', methods=['GET'])
@login_required(required_role='admin')
def user_management():
    user_list = db.session.query(User).all()
    return render_template('user_management.html', user=current_user, user_list=user_list)


@server_app.route('/create_user', methods=['POST'])
@login_required(required_role='admin')
def create_user():
    try:
        username = request.form.get('username')
        email = request.form.get('email')
        password = request.form.get('password')
        role = request.form.get('role')

        if db.session.query(exists().where(email == User.email)).scalar():
            data = {'error': gettext('ERROR_EMAIL_ALREADY_EXISTS')}
            return make_response(jsonify(data), 409)
        elif db.session.query(exists().where(username == User.username)).scalar():
            data = {'error': gettext('ERROR_USERNAME_ALREADY_EXISTS')}
            return make_response(jsonify(data), 409)

        user = User(username, email, password, role)
        db.session.add(user)
        db.session.commit()
        return 'success'
    except Exception as e:
        abort(500, e)


@server_app.route('/delete_user', methods=['POST'])
@login_required(required_role='admin')
def delete_user():
    try:
        user_id = request.form.get('id')
        if user_id == 1:
            data = {'error': gettext('ERROR_CANNOT_DELETE_SUPERADMIN_ACCOUNT')}
            return make_response(jsonify(data), 403)
        user = db.session.query(User).filter(User.id == user_id).first()
        if not user:
            data = {'error': gettext('ERROR_USER_ALREADY_DELETED')}
            return make_response(jsonify(data), 410)
        db.session.delete(user)
        db.session.commit()
        return 'success'
    except Exception as e:
        abort(500, e)


@server_app.route('/account_settings', methods=['GET'])
@login_required(required_role='any')
def account_settings():
    return render_template('account_settings.html', user=current_user)


@server_app.route('/account_settings', methods=['POST'])
@login_required(required_role='any')
def change_account_settings():
    try:
        old_password = request.form.get('old_password')
        password = request.form.get('password')
        password2 = request.form.get('password2')
        email = request.form.get('email')
        if not current_user.check_password(old_password):
            data = {'error': gettext('ERROR_WRONG_PASSWORD')}
            return make_response(jsonify(data), 401)

        if password == '':
            data = {'error': gettext('ERROR_NEW_PASSWORD_TOO_SHORT')}
            return make_response(jsonify(data), 409)
        elif password != password2:
            data = {'error': gettext('ERROR_PASSWORDS_DONT_MATCH')}
            return make_response(jsonify(data), 409)
        else:
            current_user.set_password(password)

        if email != '' and not db.session.query(exists().where(email == User.email)).scalar():
            current_user.email = email
        else:
            db.session.rollback()
            data = {'error': gettext('ERROR_EMAIL_ALREADY_EXISTS')}
            return make_response(jsonify(data), 401)
        db.session.commit()
        return 'success'
    except Exception as e:
        abort(500, e)


@server_app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('annotate'))
    if request.method == 'POST':
        try:
            user = db.session.query(User).filter(User.username == request.form.get('username')).first()
            if user is None or not user.check_password(request.form.get('password')):
                data = {'error': gettext('ERROR_INVALID_USERNAME_OR_PASSWORD')}
                return make_response(jsonify(data), 401)
            else:
                login_user(user)
                return 'success'
        except Exception as e:
            abort(500, e)
    return render_template('login.html', user=current_user)


@server_app.route('/logout', methods=['GET'])
def logout():
    logout_user()
    return redirect(url_for('search'))

# ----------------- END ROUTES -----------------------


# ------------ HELPER FUNCTIONS ---------------

# Parses the comma separated parameters (URL/a=b,c=d...) into a dictionary ({a: b, c: d, ...})
def parse_parameters(parameters):
    parameter_dictionary = {}
    parameter_list = parameters.split(',')
    for parameter in parameter_list:
        name, value = parameter.split('=')
        parameter_dictionary[name] = value
    return parameter_dictionary


# Get the settings from the database as a dictionary ({name: value, name: value, ...)
def load_settings():
    setting_dictionary = {}
    settings = db.session.query(ServerSetting).all()
    for setting in settings:
        name = setting.name
        value = setting.value
        setting_dictionary[name] = value
    return setting_dictionary

# ------------ END HELPER FUNCTIONS ---------------
