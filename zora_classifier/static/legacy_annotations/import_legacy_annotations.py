import json
import os
import pandas as pd

# Load new annotations csv
new_annotations_df = pd.read_csv('legacy_annotations.csv', encoding='UTF-8')
new_annotations_df.columns = map(str.lower, new_annotations_df.columns)
new_annotations_df = new_annotations_df.dropna()

# Create list of new legacy annotations
new_annotations = []
for index, row in new_annotations_df.iterrows():

    # Get classification value
    if 'classification' in new_annotations_df.columns:
        classification_raw = row['classification']
    else:
        raise Exception('No "classification" column found.')
    if type(classification_raw) is int:
        if classification_raw != 0 and classification_raw != 1:
            raise Exception('Classification values must be either 0 or 1')
        classification = bool(classification_raw)
    else:
        raise Exception('Classification values must be either 0 or 1.')

    # Get UID
    if 'uid' in new_annotations_df.columns:
        uid = row['uid']
    elif 'url' in new_annotations_df.columns:
        paper_id = row['url'].split('https://www.zora.uzh.ch/id/eprint/', 1)[1].split('/')[0]
        uid = 'oai:www.zora.uzh.ch:' + paper_id
    else:
        raise Exception('No "uid" or "url" column found.')

    # Add entry to list
    new_annotations.append({'uid': uid, 'classification': classification, 'annotated': True})

# Load existing annotations and merge with new annotations
annotations = []
count = 0
if not os.stat('legacy_annotations.json').st_size == 0:
    with open('legacy_annotations.json', 'r') as json_file:
        annotations = json.load(json_file)
        for new_annotation in new_annotations:
            if not any(annotation['uid'] == new_annotation['uid'] for annotation in annotations):
                annotations.append(new_annotation)
                count += 1
else:
    annotations.extend(new_annotations)

# Write merged annotations list back to file
with open('legacy_annotations.json', 'w') as json_file:
    json.dump(annotations, json_file)

print(str(count) + ' new annotations added. Total annotations: ' + str(len(annotations)))
