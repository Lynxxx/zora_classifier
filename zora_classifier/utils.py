import locale

from zora_classifier import server_app, babel
from datetime import datetime
from flask import current_app, session, request, url_for
from flask_login import current_user
from functools import wraps


# Debugging
def is_debug():
    return server_app.config['DEBUG']


# This function overwrites the login_required decorator of flask_login. It gives us the ability to distinguish between
# normal users that can only annotate and admins that may change settings and create new users.
def login_required(required_role='any'):
    def wrapper(fn):
        @wraps(fn)
        def inner_fn(*args, **kwargs):
            if not current_user.is_authenticated:
                return current_app.login_manager.unauthorized()
            current_app.login_manager.reload_user()
            user_role = current_user.get_user_role()
            if (required_role != 'any') and (user_role != required_role):
                return current_app.login_manager.unauthorized()
            return fn(*args, **kwargs)
        return inner_fn
    return wrapper


# To let the user choose the display language, we need to override get_locale
@babel.localeselector
def get_locale():
    return session.get('lang')


# Jinja2 settings and injections
def create_creator_string(creators):
    first = True
    creator_string = ''
    for creator in creators:
        if first:
            creator_string = creator.first_name + ' ' + creator.last_name
            first = False
        else:
            if creator is not None:
                if creator.first_name is not None:
                    creator_string = creator_string + ', ' + creator.first_name + ' ' + creator.last_name
    return creator_string


def url_for_other_page(page, filters):
    args = request.view_args.copy()
    args['page'] = page
    if filters is not None:
        args.update(filters)
    return url_for(request.endpoint, **args)


server_app.jinja_env.globals.update(create_creator_string=create_creator_string)
server_app.jinja_env.globals.update(url_for_other_page=url_for_other_page)


@server_app.context_processor
def inject_utils():
    def format_number(number):
        number = float(number)
        return locale.format_string("%d", number, grouping=True)
    return {'now': datetime.utcnow(),
            'format_number': format_number}
