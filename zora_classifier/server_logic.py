import json
import os

import pandas as pd
import random

from datetime import datetime, timedelta
from flask_apscheduler import APScheduler
from flask_sqlalchemy import event
from threading import Timer

from zora_classifier import db, server_app
from zora_classifier.models import Paper, Institute, ResourceType, ServerSetting, OperationParameter, GeneralStatistic, \
    Top10Statistic, Creator, Keyword, DDC, GraphStatistic
from zora_classifier.ml_tool import MLTool
from zora_classifier.utils import is_debug
from zora_classifier.zoraAPI import ZoraAPI


class ServerLogic:
    ZORA_API_JOB_ID = 'zoraAPI_get_records_job'
    INSTITUTE_UPDATE_JOB_ID = 'institute_update_job'
    RESOURCE_TYPE_UPDATE_JOB_ID = 'resource_type_update_job'
    STATISTIC_UPDATE_JOB_ID = 'statistic_update_job'

    # The __init__ method is used to initialize the server logic
    def __init__(self):

        # Initialize the list of papers that are being annotated currently
        self.lockedPapers = []

        # Initialize the ZORA API
        url = ServerSetting.get('zora_url')
        self.zoraAPI = ZoraAPI(url)
        print('ZORA API initialized')

        # Initialize the institutes if needed
        if not OperationParameter.get('last_institute_update'):
            # Load the institutes from ZORA
            self.load_institutes()
            print('Institutes loaded')

            # Initialize the resource types if needed
        if not OperationParameter.get('last_resource_type_update'):
            # Load the resource types from ZORA
            self.load_resource_types()
            print('Resource types loaded')

        # Import the legacy annotations
        file_path = server_app.config['LEGACY_ANNOTATIONS_PATH']
        self.import_legacy_annotations(file_path)

        # Initialize the machine learning tool
        self.ml_tool = MLTool(server_app.config['ML_MODEL_FOLDER'])

        # Initialize the task scheduler
        self.scheduler = APScheduler()
        self.scheduler.init_app(server_app)
        self.scheduler.start()
        print('Task scheduler initialized')

        # Initialize the institute update job, which updates the list of institutes
        job_interval = ServerSetting.get('institute_update_interval')
        next_institute_update_run = OperationParameter.get('last_institute_update') + timedelta(days=job_interval)
        if next_institute_update_run < datetime.now():
            next_institute_update_run = datetime.now()
        self.scheduler.add_job(func=self.load_institutes,
                               trigger='interval',
                               days=job_interval,
                               next_run_time=next_institute_update_run,
                               id=ServerLogic.INSTITUTE_UPDATE_JOB_ID)
        print('Institute update job started')

        # Initialize the resource_type update job, which updates the list of resource_types
        job_interval = ServerSetting.get('resource_type_update_interval')
        next_resource_type_update_run = OperationParameter.get('last_resource_type_update') + timedelta(days=job_interval)
        if next_resource_type_update_run < datetime.now():
            next_resource_type_update_run = datetime.now()
        self.scheduler.add_job(func=self.load_resource_types,
                               trigger='interval',
                               days=job_interval,
                               next_run_time=next_resource_type_update_run,
                               id=ServerLogic.RESOURCE_TYPE_UPDATE_JOB_ID)
        print('Resource type update job started')

        # Initialize the zora pull job, that pulls data from the ZORA repository in a fixed interval
        job_interval = ServerSetting.get('zora_pull_interval')
        last_zora_pull = OperationParameter.get('last_zora_pull')
        next_zora_pull_run = datetime.now()
        if last_zora_pull:
            next_zora_pull_run = last_zora_pull - timedelta(days=job_interval)
        if next_zora_pull_run < datetime.now():
            next_zora_pull_run = datetime.now()
        self.scheduler.add_job(func=self.zora_pull,
                               trigger='interval',
                               days=job_interval,
                               next_run_time=next_zora_pull_run,
                               id=ServerLogic.ZORA_API_JOB_ID)
        print('ZORA pull job started')

        # Initialize the statistic update job, which updates the statistics
        self.scheduler.add_job(func=self.update_statistics,
                               trigger='interval',
                               days=1,
                               next_run_time=datetime.now(),
                               id=ServerLogic.STATISTIC_UPDATE_JOB_ID)
        print('Statistic update job started')

        # Register the database event listener for the server settings table
        @event.listens_for(ServerSetting.value, 'set')
        def handle_setting_change(target, value, oldvalue, initiator):
            self.handle_setting_change(target, value, oldvalue, initiator)
        print('Database event handler registered')

        print('Server initialized')

    # This function gets the latest papers from ZORA, which are then classified and stored in the database.
    def zora_pull(self):

        # We want to store the starting time to update last_zora_pull when we are done
        new_last_zora_pull = datetime.utcnow()

        # Get the papers that were created or updated since the last pull
        from_ = OperationParameter.get('last_zora_pull')
        metadata_dict_list = self.zoraAPI.get_metadata_dicts(from_)

        count = 0
        print('Storing papers...')
        for index, metadata_dict in enumerate(metadata_dict_list):

            # Classify the paper based on title and description. If we don't have a model the paper won't be classified
            title = metadata_dict['title'] if 'title' in metadata_dict and metadata_dict['title'] else ''
            description = metadata_dict['description'] if 'description' in metadata_dict and metadata_dict['description'] else ''
            text = title + ' | ' + description
            prediction = self.ml_tool.classify(text)
            if prediction is not None:
                metadata_dict['classification'] = prediction

            # Create or update the paper
            Paper.create_or_update(metadata_dict)

            if index % 10000 == 0:
                db.session.commit()

            if is_debug():
                count += 1
                if is_debug() and count % 10000 == 0:
                    print('Count: ' + str(count))
        print(count)
        print('Done')

        # After the zora_pull is completed, we update the last_zora_pull operation parameter, so that we can only get
        # the most recent changes of the ZORA repository. Then commit the transaction
        OperationParameter.set('last_zora_pull', new_last_zora_pull)
        db.session.commit()

        if is_debug():
            print('Duration: ' + str(datetime.utcnow() - new_last_zora_pull))

    # This method loads all legacy annotations from the legacy_annotations.json if they are not loaded already
    def import_legacy_annotations(self, file_path):

        # Check if we already imported the legacy annotations
        if OperationParameter.get('legacy_annotations_imported'):
            print('Legacy annotations already imported')
            return

        # Check if there exist any legacy annotations
        if not os.path.isfile(file_path):
            print('No legacy annotations found')
            return

        # Load the legacy annotations from the json file defined in the config.py
        with open(file_path, 'rt') as file:
            paper_dict_list = json.load(file)

        # Import all legacy annotations
        print('Importing legacy annotations...')
        count = 0
        for paper_dict in paper_dict_list:

            # Check if the paper already exists in the database. If it does, we only want to set the classification and
            # annotated values (since we can assume that the other existing values are more recent). Otherwise we
            # create a new entry in the database.
            paper = db.session.query(Paper).get(paper_dict['uid'])
            if paper:
                paper.classification = paper_dict['classification']
                paper.annotated = paper_dict['annotated']
            else:
                paper_record_dict = self.zoraAPI.get_metadata_dict(paper_dict['uid'])
                if not paper_record_dict:
                    continue
                paper_dict.update(paper_record_dict)
                paper = Paper.create_or_update(paper_dict)
                db.session.add(paper)
            count += 1
            if is_debug() and count % 100 == 0:
                print('Count: ' + str(count))

        # Update legacy_annotations_imported so we know we don't have to import them anymore on a server startup.
        # Then commit the transaction.
        OperationParameter.set('legacy_annotations_imported', True)
        db.session.commit()

        print('Legacy annotations imported')

    # Loads the institutes from ZORA and stores them in the database
    def load_institutes(self):
        institute_name_dict = self.zoraAPI.get_institutes()
        for institute_name, children_dict in institute_name_dict.items():
            self.store_institute_hierarchy(institute_name, children_dict, None)
        OperationParameter.set('last_institute_update', datetime.utcnow())
        db.session.commit()

    # A recursive method that explores the tree structure of the institutes dictionary and stores the institutes with
    # their corresponding parent institute.
    def store_institute_hierarchy(self, current_name, children_dict, parent):
        current_institute = db.session.query(Institute).filter(Institute.name == current_name, Institute.parent == parent).first()
        if not current_institute:
            current_institute = Institute(current_name)
            current_institute.parent = parent
            db.session.add(current_institute)
        if children_dict:
            for child_name, child_children_dict in children_dict.items():
                self.store_institute_hierarchy(child_name, child_children_dict, current_institute)

    # Loads the resource_types from ZORA and stores them in the database
    def load_resource_types(self):
        resource_type_list = self.zoraAPI.get_resource_types()
        for resource_type in resource_type_list:
            ResourceType.get_or_create(resource_type)
        OperationParameter.set('last_resource_type_update', datetime.utcnow())
        db.session.commit()

    # Update the statistics and store them in the database
    @staticmethod
    def update_statistics():
        general_statistics = Paper.get_paper_statistics()
        for name, value in general_statistics.items():
            GeneralStatistic.set(name, value)

        top10_authors = Creator.get_top10_authors()
        Top10Statistic.set('authors', top10_authors)
        top10_keywords = Keyword.get_top10_keywords()
        Top10Statistic.set('keywords', top10_keywords)
        top10_ddcs = DDC.get_top10_ddcs()
        Top10Statistic.set('ddcs', top10_ddcs)
        top10_institutes = Institute.get_top10_institutes()
        Top10Statistic.set('institutes', top10_institutes)

        html_plot = Paper.get_positive_papers_per_year()
        GraphStatistic.set('positive_papers_per_year', html_plot)
        db.session.commit()

    # This method handles changes to the settings.
    # zora_pull_interval:   Reschedules the zora_pull_job with the new interval
    # zora_url:             Creates a new connection to the ZORA API with the new URL
    def handle_setting_change(self, target, value, oldvalue, initiator):
        setting_name = target.name

        if setting_name == 'zora_pull_interval':

            # Change the interval of the zora api job
            job1 = self.scheduler.get_job(id=ServerLogic.ZORA_API_JOB_ID)
            if job1:
                job1.reschedule(trigger='interval', days=value)
        elif setting_name == 'institute_update_interval':

            # Change the interval of the institute updates
            job2 = self.scheduler.get_job(id=ServerLogic.INSTITUTE_UPDATE_JOB_ID)
            if job2:
                job2.reschedule(trigger='interval', days=value)
        elif setting_name == 'resource_type_update_interval':

            # Change the interval of the institute updates
            job3 = self.scheduler.get_job(id=ServerLogic.RESOURCE_TYPE_UPDATE_JOB_ID)
            if job3:
                job3.reschedule(trigger='interval', days=value)
        elif setting_name == 'zora_url':

            # Create a new connection with the new url
            self.zoraAPI = self.zoraAPI = ZoraAPI(value)

        if is_debug():
            print('Setting "' + setting_name + '" was changed to ' + str(value) + '.')

    # Picks a paper from all papers that are not yet annotated and not currently being annotated
    def get_annotation(self):
        query = db.session.query(Paper).filter(Paper.annotated == False, Paper.description != None, Paper.uid.notin_(self.lockedPapers))
        row_count = int(query.count())
        paper = query.offset(int(row_count*random.random())).first()
        if not paper:
            return None
        self.lockedPapers.append(paper.uid)
        annotation_timeout = ServerSetting.get('annotation_timeout')
        timer = Timer(annotation_timeout, self.timeout_annotation, [paper.uid])
        timer.start()
        return paper

    # Sets the annotated and classification properties of a paper based on how it got annotated
    def set_annotation(self, uid, classification):
        if uid in self.lockedPapers:
            self.lockedPapers.remove(uid)
            if classification is not None:
                paper = db.session.query(Paper).get(uid)
                paper.classification = classification
                paper.annotated = True
                db.session.commit()
            return 200
        else:
            return 408

    # Removes a paper from the locked list but does not set it to annotated
    def skip_annotation(self, uid):
        if uid in self.lockedPapers:
            self.lockedPapers.remove(uid)
            return 200
        else:
            return 408

    # Timeouts annotations and removes them from the list of currently processed papers when they take too long.
    def timeout_annotation(self, uid):
        if uid in self.lockedPapers:
            self.lockedPapers.remove(uid)

    # Creates a new model based on all currently annotated papers and classifies all the papers again.
    def create_new_model(self):

        # Reset the machine learning model
        self.ml_tool = MLTool(server_app.config['ML_MODEL_FOLDER'])

    def classify_papers(self):
        if is_debug():
            print('classifying papers...')

        # Prepare the data
        papers = pd.read_sql_query(db.session.query(Paper).filter(Paper.annotated == False).statement, db.session.bind)
        papers['title'].fillna('', inplace=True)
        papers['description'].fillna('', inplace=True)

        # TODO: With or without title?
        predictions = self.ml_tool.classify_batch(papers["title"] + " | " + papers["description"])

        # Update all classifications and commit after each 1000 papers to avoid locking the database for too long
        if predictions is not None:
            count = 0
            for index, prediction in enumerate(predictions):
                paper = db.session.query(Paper).get(papers.loc[index, 'uid'])
                paper.classification = prediction
                if index % 1000 == 0:
                    db.session.commit()
                if is_debug():
                    count += 1
                    if is_debug() and count % 1000 == 0:
                        print('Count: ' + str(count))
            db.session.commit()

        if is_debug():
            print('Done')

    # Loads the newly uploaded model and classifies all papers anew.
    def apply_trained_model(self):
        self.create_new_model()
        self.classify_papers()
