from flask import Flask
from flask_login import LoginManager
from flask_sqlalchemy import SQLAlchemy
from flask_babel import Babel

# Create the app
server_app = Flask(__name__)

# Load the config
server_app.config.from_object('config')

# Load the database
db = SQLAlchemy(server_app)

# Initialize the LoginManager
login_manager = LoginManager(server_app)

# Initialize babel
babel = Babel(server_app)

# NOTE: These imports are not at the top of the file to avoid circular imports (we need server_app)
from zora_classifier import models, server_logic

# Initialize the database
models.initialize_db()

# Initialize the zora_classifier logic, which includes the ZORA API, machine learning tool, task scheduler and jobs
server_logic_instance = server_logic.ServerLogic()

# NOTE routes is imported last because it needs all the initialized variables
from zora_classifier import routes