import numpy as np
import pandas as pd

from fast_bert.prediction import BertClassificationPredictor


# The MLTool class loads a fine-tuned model and allows us to classify one or multiple papers.
class MLTool:
    def __init__(self, model_path):

        # Load the fine-tuned model
        try:
            self.predictor = BertClassificationPredictor(model_path=model_path,
                                                         label_path=model_path,
                                                         multi_label=False,
                                                         model_type='bert',
                                                         do_lower_case=False)
            self.has_model = True
        except Exception:
            self.has_model = False

    # This method classifies the given papers by the data provided
    def classify_batch(self, data: pd.Series):
        if not self.has_model or len(data) == 0:
            return None

        output = self.predictor.predict_batch(list(data))
        predictions = pd.DataFrame([{item[0]: item[1] for item in pred} for pred in output])
        classifications = np.where(predictions['1'] >= predictions['0'], True, False)

        return classifications

    # This method classifies the given paper by the data provided
    def classify(self, text):
        if not self.has_model or len(text) == 0:
            return None

        output = self.predictor.predict(text)
        prediction = {item[0]: item[1] for item in output}
        classification = True if prediction['1'] >= prediction['0'] else False
        return classification
