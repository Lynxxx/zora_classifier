APScheduler==3.6.1
Babel==2.7.0
Click==7.0
cycler==0.10.0
Flask==1.1.1
Flask-APScheduler==1.11.0
Flask-Babel==0.12.2
Flask-Login==0.4.1
Flask-SQLAlchemy==2.4.1
itsdangerous==1.1.0
Jinja2==2.10.3
joblib==0.14.0
kiwisolver==1.1.0
lxml==4.4.1
MarkupSafe==1.1.1
matplotlib==3.0.3
mpld3==0.3
numpy==1.17.3
pandas==0.25.2
pyoai==2.5.0
pyparsing==2.4.2
python-dateutil==2.8.0
pytz==2019.3
scikit-learn==0.22.1
scipy==1.3.1
six==1.12.0
SQLAlchemy==1.3.10
torch
torchvision
# fast-bert ist listed here because it depends on torch
fast-bert
tzlocal==2.0.0
Werkzeug==0.16.0
