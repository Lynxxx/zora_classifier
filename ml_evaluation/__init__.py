import config
import pandas as pd
import sklearn

from ml_evaluation.classifier_tester import ClassifierTester
from ml_evaluation.evaluator import Evaluator
from ml_evaluation.vectorizer_factory import VectorizerFactory
from sqlalchemy import create_engine, MetaData, Table
from sqlalchemy.orm import sessionmaker, mapper
from warnings import simplefilter

SEED = 7

# Ignore all future warnings
simplefilter(action='ignore', category=FutureWarning)


# Dummy Paper class to load papers from the ZORA classifier database
class Paper(object):
    pass


# Function to connect to the ZORA classifier database
def load_session():
    engine = create_engine(config.SQLALCHEMY_DATABASE_URI)
    metadata = MetaData(engine)
    papers = Table('papers', metadata, autoload=True)
    mapper(Paper, papers)
    Session = sessionmaker(bind=engine)
    return Session()

# This script tests a variety of machine learning algorithms on the data of the ZORA classifier.
# Be aware that you need to set the different parameters and download the pretrained_models.
if __name__ == '__main__':

    # Load all sustainable papers with english abstracts from the database
    session = load_session()
    sustainable_papers_query = session.query(Paper).filter(Paper.annotated == True).filter(
        Paper.description != None).filter(Paper.language_id == 1).statement
    data_raw = pd.read_sql(sustainable_papers_query, session.bind)

    # Prepare the data
    data = pd.DataFrame()
    data['uid'] = data_raw['uid']
    data['abstract'] = data_raw["title"] + " | " + data_raw["description"]
    data['label'] = data_raw['classification']

    # Split the dataset into train and test sets
    X_train, X_test, y_train, y_test = sklearn.model_selection.train_test_split(data['abstract'], data['label'],
                                                                                train_size=0.8, random_state=SEED)
    # Print how skewed the sets are.
    print('Train set skewness')
    print('Total entries: ' + str(len(y_train)))
    print(y_train.value_counts())
    print('Test set skewness')
    print('Total entries: ' + str(len(y_test)))
    print(y_test.value_counts())

    # These dictionaries defines which parameter combinations are being used
    # Since neural network classifiers need completely different parameters, they have a separate dictionary
    parameter_space = {
        'vectorizer': ['count', 'hashing', 'tf_idf'],
        'tokenizer': ['sklearn'],
        'dimensionality_reduction': [None, 'lsa'],
        'dimensionality': [200],
        'n_gram': [1, 2, 3],
        'stop_words': [True, False],
        'lemmatization': [False], # Not yet implemented
        'case_folding': [True, False]
    }

    # Chose which tests are being run
    use_classifier = {
        'multinomial_naive_bayes': False,
        'gaussian_naive_bayes': False,
        'k_nearest_neighbors': False,
        'support_vector': False,
        'gaussian_process': False,
        'decision_tree': False,
        'random_forest': False,
        'multi_layer_perceptron': False,
        'keras': False,
        'bert': False,  # Only prepare the data. Needs a strong GPU.
        'ulmfit': False,
        'fasttext': False
    }

    classifier_tester = ClassifierTester(data=data, X_test=X_test, X_train=X_train, y_train=y_train, y_test=y_test,
                                         use_classifier=use_classifier, params=parameter_space, seed=SEED)
    results = classifier_tester.test_all()
    evaluator = Evaluator(results)
    evaluator.evaluate_all()
