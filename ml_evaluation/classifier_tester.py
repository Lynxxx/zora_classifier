import fasttext
import itertools
import nltk
import numpy as np
import os
import pandas as pd
import random
import re
import tensorflow as tf
import tensorflow_hub as hub
import torch

from fastai.text import accuracy, AWD_LSTM, BaseTokenizer, Collection, collections, error_rate, \
    language_model_learner, Learner, List, nn, NumericalizeProcessor, text_classifier_learner, TextClasDataBunch, \
    TextList, TextLMDataBunch, Tokenizer, TokenizeProcessor, Vocab
from functools import partial
from keras import Sequential
from keras.layers import Dense
from keras.wrappers.scikit_learn import KerasClassifier
from ml_evaluation.vectorizer_factory import VectorizerFactory
from nltk.corpus import stopwords
from sklearn.ensemble import RandomForestClassifier
from sklearn.gaussian_process import GaussianProcessClassifier
from sklearn.gaussian_process.kernels import ConstantKernel, ExpSineSquared, RationalQuadratic, RBF
from sklearn.metrics import confusion_matrix, roc_curve
from sklearn.model_selection import GridSearchCV, train_test_split
from sklearn.naive_bayes import MultinomialNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from transformers import AdamW, PreTrainedModel, PreTrainedTokenizer, \
    BertForSequenceClassification, BertTokenizer, BertConfig, \
    DistilBertForSequenceClassification, DistilBertTokenizer, DistilBertConfig, \
    RobertaForSequenceClassification, RobertaTokenizer, RobertaConfig, \
    XLMForSequenceClassification, XLMTokenizer, XLMConfig, \
    XLNetForSequenceClassification, XLNetTokenizer, XLNetConfig


# This class allows the testing of various classifiers with train data on test data over a parameter space
class ClassifierTester:
    def __init__(self, data, X_train, X_test, y_train, y_test, use_classifier, params, seed):
        self.result_list = []

        # Store the different sets of data
        self.data = data
        self.X_train = X_train
        self.X_test = X_test
        self.y_train = y_train
        self.y_test = y_test

        self.use_classifier = use_classifier
        self.SEED = seed

        # Create all vectorizers for the parameters space
        params_list = self.generate_params_list(params)
        self.vectorizer_list = []
        print('Creating vectorizers...')
        for index, params in enumerate(params_list, start=1):
            vectorizer = VectorizerFactory(data=X_train,
                                           vectorizer=params['vectorizer'],
                                           tokenizer=params['tokenizer'],
                                           dimensionality_reduction=params['dimensionality_reduction'],
                                           dimensionality=params['dimensionality'],
                                           n_gram=params['n_gram'],
                                           stop_words=params['stop_words'],
                                           lemmatization=params['lemmatization'],
                                           case_folding=params['case_folding'])
            self.vectorizer_list.append(vectorizer)
            print(str(index) + '/' + str(len(params_list)))

        # Check if a GPU device is found
        # NOTE: We need a GPU to train the neural networks efficiently
        device_name = tf.test.gpu_device_name()
        if device_name != '/device:GPU:0':
            print('GPU device not found')
            raise SystemError('GPU device not found')
        print('Found GPU at: {}'.format(device_name))

        # Prepare the list of stop words
        nltk.download('stopwords')

    def test_all(self):
        print('Testing standard classifiers...')
        for index, vectorizer in enumerate(self.vectorizer_list, start=1):
            print('Vectorizer ' + str(index) + '/' + str(len(self.vectorizer_list)))
            if self.use_classifier['multinomial_naive_bayes']:
                self.test_multinomial_naive_bayes(vectorizer)
                print('Multinomial Naive Bayes Done')
            if self.use_classifier['gaussian_naive_bayes']:
                self.test_gaussian_naive_bayes(vectorizer)
                print('Gaussian Naive Bayes Done')
            if self.use_classifier['k_nearest_neighbors']:
                self.test_k_nearest_neighbors(vectorizer)
                print('K Nearest Neighbor Done')
            if self.use_classifier['support_vector']:
                self.test_support_vector(vectorizer)
                print('Support Vector Done')
            if self.use_classifier['gaussian_process']:
                self.test_gaussian_process(vectorizer)
                print('Gaussian Process Done')
            if self.use_classifier['decision_tree']:
                self.test_decision_tree(vectorizer)
                print('Decision Tree Done')
            if self.use_classifier['random_forest']:
                self.test_random_forest(vectorizer)
                print('Random Forest Done')
            if self.use_classifier['multi_layer_perceptron']:
                self.test_multi_layer_perceptron(vectorizer)
                print('Multi-Layer Perceptron Done')
        print('Testing non-sklearn classifiers...')
        if self.use_classifier['bert']:
            self.test_bert()
            print('Bert Done')
        if self.use_classifier['keras']:
            self.test_keras(vectorizer)
            print('Keras Done')
        if self.use_classifier['ulmfit']:
            self.test_ulmfit()
            print('ULMFiT Done')
        if self.use_classifier['fasttext']:
            self.test_fasttext()
            print('Fasttext Done')

        return self.result_list

    # MULTINOMIAL NAIVE BAYES CLASSIFIER
    def test_multinomial_naive_bayes(self, vectorizer):

        # Multinomial Naive Bayes does not accept negative values. Therefore we can't use lsa dimensionality reduction
        # or hashing vectorizer
        if vectorizer.parameters['dimensionality_reduction'] == 'lsa' or vectorizer.parameters['vectorizer'] == 'hashing':
            return

        # Train
        classifier = MultinomialNB()

        # Search for the best hyperparameters
        parameter_space = {
            'alpha': [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1],
            'fit_prior': [True, False]
        }
        classifier = GridSearchCV(classifier, parameter_space, n_jobs=3, cv=3)
        vocabulary_dtm = vectorizer.get_vocabulary_dtm()
        classifier.fit(vocabulary_dtm, self.y_train)

        # Test
        test_dtm = vectorizer.get_data_dtm(self.X_test)
        predictions_test = classifier.predict(test_dtm)
        probabilities_test = classifier.predict_proba(test_dtm)
        probabilities_test = probabilities_test[:, 1]

        # Create result
        tn, fp, fn, tp = confusion_matrix(self.y_test, predictions_test).ravel()
        conf_matrix = {
            'tp': tp,
            'fp': fp,
            'fn': fn,
            'tn': tn
        }
        result = {
            'classifier': 'multinomial_naive_bayes',
            'parameters': {
                'vectorizer': vectorizer.parameters,
                'classifier': classifier.best_params_
            },
            'confusion_matrix': conf_matrix,
            'roc': self.get_roc(probabilities_test)
        }
        self.result_list.append(result)

    # GAUSSIAN NAIVE BAYES CLASSIFIER
    def test_gaussian_naive_bayes(self, vectorizer):
        # Train
        classifier = RandomForestClassifier()
        vocabulary_dtm = vectorizer.get_vocabulary_dtm()
        classifier.fit(vocabulary_dtm, self.y_train)

        # Test
        test_dtm = vectorizer.get_data_dtm(self.X_test)
        predictions_test = classifier.predict(test_dtm)
        probabilities_test = classifier.predict_proba(test_dtm)
        probabilities_test = probabilities_test[:, 1]

        # Create result
        tn, fp, fn, tp = confusion_matrix(self.y_test, predictions_test).ravel()
        conf_matrix = {
            'tp': tp,
            'fp': fp,
            'fn': fn,
            'tn': tn
        }
        result = {
            'classifier': 'gaussian_naive_bayes',
            'parameters': {
                'vectorizer': vectorizer.parameters,
                'classifier': 'sklearn defaults'
            },
            'confusion_matrix': conf_matrix,
            'roc': self.get_roc(probabilities_test)
        }
        self.result_list.append(result)

    # K NEAREST NEIGHBORS CLASSIFIER
    def test_k_nearest_neighbors(self, vectorizer):

        # Train
        classifier = KNeighborsClassifier()

        # Search for the best hyperparameters
        parameter_space = {
            'n_neighbors': [1, 2, 3, 5, 7, 11],
            'weights': ['uniform', 'distance']
        }
        classifier = GridSearchCV(classifier, parameter_space, n_jobs=3, cv=3)
        vocabulary_dtm = vectorizer.get_vocabulary_dtm()
        classifier.fit(vocabulary_dtm, self.y_train)

        # Test
        test_dtm = vectorizer.get_data_dtm(self.X_test)
        predictions_test = classifier.predict(test_dtm)
        probabilities_test = classifier.predict_proba(test_dtm)
        probabilities_test = probabilities_test[:, 1]

        # Create result
        tn, fp, fn, tp = confusion_matrix(self.y_test, predictions_test).ravel()
        conf_matrix = {
            'tp': tp,
            'fp': fp,
            'fn': fn,
            'tn': tn
        }
        result = {
            'classifier': 'k_nearest_neighbors',
            'parameters': {
                'vectorizer': vectorizer.parameters,
                'classifier': classifier.best_params_
            },
            'confusion_matrix': conf_matrix,
            'roc': self.get_roc(probabilities_test)
        }
        self.result_list.append(result)

    # SUPPORT VECTOR CLASSIFIER
    def test_support_vector(self, vectorizer):
        # Train
        classifier = SVC(kernel='linear', probability=True)

        # Search for the best hyperparameters
        parameter_space = {
            'C': [0.001, 0.01, 0.1, 1, 10],
            'kernel': ['linear', 'poly', 'rbf', 'sigmoid'],
            'gamma': [0.001, 0.01, 0.1, 1]
        }
        classifier = GridSearchCV(classifier, parameter_space, n_jobs=3, cv=3)
        vocabulary_dtm = vectorizer.get_vocabulary_dtm()
        classifier.fit(vocabulary_dtm, self.y_train)

        # Test
        test_dtm = vectorizer.get_data_dtm(self.X_test)
        predictions_test = classifier.predict(test_dtm)
        probabilities_test = classifier.predict_proba(test_dtm)
        probabilities_test = probabilities_test[:, 1]

        # Create result
        tn, fp, fn, tp = confusion_matrix(self.y_test, predictions_test).ravel()
        conf_matrix = {
            'tp': tp,
            'fp': fp,
            'fn': fn,
            'tn': tn
        }
        result = {
            'classifier': 'support_vector',
            'parameters': {
                'vectorizer': vectorizer.parameters,
                'classifier': classifier.best_params_
            },
            'confusion_matrix': conf_matrix,
            'roc': self.get_roc(probabilities_test)
        }
        self.result_list.append(result)

    # GAUSSIAN PROCESS CLASSIFIER
    def test_gaussian_process(self, vectorizer):

        # Gaussian process classifier seems not to work with negative values (takes seemingly infinite amount of time to
        # process). Therefore we can't use lsa dimensionality reduction or hashing vectorizer.
        if vectorizer.parameters['dimensionality_reduction'] == 'lsa' or vectorizer.parameters[
            'vectorizer'] == 'hashing':
            return

        # Train
        classifier = GaussianProcessClassifier()

        # Search for the best hyper parameters
        ker_rbf = ConstantKernel(1.0, constant_value_bounds="fixed") * RBF(1.0, length_scale_bounds="fixed")
        ker_rq = ConstantKernel(1.0, constant_value_bounds="fixed") * RationalQuadratic(alpha=0.1, length_scale=1)
        ker_expsine = ConstantKernel(1.0, constant_value_bounds="fixed") * ExpSineSquared(1.0, 5.0, periodicity_bounds=(1e-2, 1e1))
        kernel_list = [ker_rbf, ker_rq, ker_expsine]

        parameter_space = {
            'kernel': kernel_list,
            'n_restarts_optimizer': [0, 1, 2, 3]
        }
        classifier = GridSearchCV(classifier, parameter_space, n_jobs=3, cv=3)
        vocabulary_dtm = vectorizer.get_vocabulary_dtm()
        classifier.fit(vocabulary_dtm.todense(), self.y_train)

        # Test
        test_dtm = vectorizer.get_data_dtm(self.X_test)
        predictions_test = classifier.predict(test_dtm.todense())
        probabilities_test = classifier.predict_proba(test_dtm.todense())
        probabilities_test = probabilities_test[:, 1]

        # Create result
        tn, fp, fn, tp = confusion_matrix(self.y_test, predictions_test).ravel()
        conf_matrix = {
            'tp': tp,
            'fp': fp,
            'fn': fn,
            'tn': tn
        }
        result = {
            'classifier': 'gaussian_process',
            'parameters': {
                'vectorizer': vectorizer.parameters,
                'classifier': None
            },
            'confusion_matrix': conf_matrix,
            'roc': self.get_roc(probabilities_test)
        }
        self.result_list.append(result)

    # DECISION TREE CLASSIFIER
    def test_decision_tree(self, vectorizer):
        # Train
        classifier = DecisionTreeClassifier()

        # Search for the best hyperparameters
        parameter_space = {
            'criterion': ['gini', 'entropy'],
            'splitter': ['best', 'random'],
            'max_depth': [None, 5, 10, 50, 200]
        }
        classifier = GridSearchCV(classifier, parameter_space, n_jobs=3, cv=3)
        vocabulary_dtm = vectorizer.get_vocabulary_dtm()
        classifier.fit(vocabulary_dtm, self.y_train)

        # Test
        test_dtm = vectorizer.get_data_dtm(self.X_test)
        predictions_test = classifier.predict(test_dtm)
        probabilities_test = classifier.predict_proba(test_dtm)
        probabilities_test = probabilities_test[:, 1]

        # Create result
        tn, fp, fn, tp = confusion_matrix(self.y_test, predictions_test).ravel()
        conf_matrix = {
            'tp': tp,
            'fp': fp,
            'fn': fn,
            'tn': tn
        }
        result = {
            'classifier': 'decision_tree',
            'parameters': {
                'vectorizer': vectorizer.parameters,
                'classifier': classifier.best_params_
            },
            'confusion_matrix': conf_matrix,
            'roc': self.get_roc(probabilities_test)
        }
        self.result_list.append(result)

    # RANDOM FOREST CLASSIFIER
    def test_random_forest(self, vectorizer):
        # Train
        classifier = RandomForestClassifier()

        # Search for the best hyperparameters
        parameter_space = {
            'n_estimators': [5, 50, 100, 200],
            'criterion': ['gini', 'entropy'],
            'max_depth': [None, 5, 10, 50, 200]
        }
        classifier = GridSearchCV(classifier, parameter_space, n_jobs=3, cv=3)
        vocabulary_dtm = vectorizer.get_vocabulary_dtm()
        classifier.fit(vocabulary_dtm, self.y_train)

        # Test
        test_dtm = vectorizer.get_data_dtm(self.X_test)
        predictions_test = classifier.predict(test_dtm)
        probabilities_test = classifier.predict_proba(test_dtm)
        probabilities_test = probabilities_test[:, 1]

        # Create result
        tn, fp, fn, tp = confusion_matrix(self.y_test, predictions_test).ravel()
        conf_matrix = {
            'tp': tp,
            'fp': fp,
            'fn': fn,
            'tn': tn
        }
        result = {
            'classifier': 'random_forest',
            'parameters': {
                'vectorizer': vectorizer.parameters,
                'classifier': classifier.best_params_
            },
            'confusion_matrix': conf_matrix,
            'roc': self.get_roc(probabilities_test)
        }
        self.result_list.append(result)

    # MULTI-LAYER PERCEPTRON CLASSIFIER
    def test_multi_layer_perceptron(self, vectorizer):

        # Multi-layer perceptron classifier seems not to work with hashing (takes seemingly infinite amount of
        # time to process). Therefore we can't use hashing vectorizer.
        if vectorizer.parameters['vectorizer'] == 'hashing':
            return

        # Train
        classifier = MLPClassifier(verbose=1, max_iter=1000)

        # Search for the best hyperparameters
        parameter_space = {
            'hidden_layer_sizes': [(50,50,50), (50,100,50), (100,)],
            'activation': ['identity', 'logistic', 'tanh', 'relu'],
            'solver': ['lbfgs', 'sgd', 'adam'],
            'alpha': [0.0001, 0.05],
            'learning_rate': ['constant', 'adaptive']
        }
        classifier = GridSearchCV(classifier, parameter_space, n_jobs=3, cv=3)
        vocabulary_dtm = vectorizer.get_vocabulary_dtm()
        classifier.fit(vocabulary_dtm, self.y_train)

        # Test
        test_dtm = vectorizer.get_data_dtm(self.X_test)
        predictions_test = classifier.predict(test_dtm)
        probabilities_test = classifier.predict_proba(test_dtm)
        probabilities_test = probabilities_test[:, 1]

        # Create result
        tn, fp, fn, tp = confusion_matrix(self.y_test, predictions_test).ravel()
        conf_matrix = {
            'tp': tp,
            'fp': fp,
            'fn': fn,
            'tn': tn
        }
        result = {
            'classifier': 'multi_layer_perceptron',
            'parameters': {
                'vectorizer': vectorizer.parameters,
                'classifier': classifier.best_params_
            },
            'confusion_matrix': conf_matrix,
            'roc': self.get_roc(probabilities_test)
        }
        self.result_list.append(result)

    # KERAS CLASSIFIER
    def test_keras(self, vectorizer):

        # Keras classifier seems not to work with hashing (takes seemingly infinite amount of
        # time to process). Therefore we can't use hashing vectorizer.
        if vectorizer.parameters['vectorizer'] == 'hashing':
            return

        def create_model():
            # Create model
            dimensionality = vectorizer.parameters['dimensionality']
            model = Sequential()
            model.add(Dense(12, input_dim=dimensionality, activation='relu'))
            model.add(Dense(8, activation='relu'))
            model.add(Dense(1, activation='sigmoid'))

            # Compile model
            model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
            return model

        # TODO: Use cross validation --> do X runs and take the mean as result
        # Fix random seed for reproducibility
        np.random.seed(self.SEED)

        # Train
        model = KerasClassifier(build_fn=create_model, epochs=150, batch_size=10, verbose=0)
        vocabulary_dtm = vectorizer.get_vocabulary_dtm()
        model.fit(vocabulary_dtm, self.y_train)

        # Test
        test_dtm = vectorizer.get_data_dtm(self.X_test)
        predictions_test = model.predict(test_dtm)
        probabilities_test = model.predict_proba(test_dtm)
        probabilities_test = probabilities_test[:, 1]

        # Create result
        tn, fp, fn, tp = confusion_matrix(self.y_test, predictions_test).ravel()
        conf_matrix = {
            'tp': tp,
            'fp': fp,
            'fn': fn,
            'tn': tn
        }
        result = {
            'classifier': 'keras',
            'parameters': {
                'vectorizer': vectorizer.parameters,
                'classifier': 'See Code'
            },
            'confusion_matrix': conf_matrix,
            'roc': self.get_roc(probabilities_test)
        }
        self.result_list.append(result)

    # FASTTEXT CLASSIFIER
    def test_fasttext(self):
        fasttext_params = {
            'epochs': [5, 20, 50, 100, 200],
            'learning_rate': [0.01, 0.1, 0.5, 1],
            'dimensionality': [300],
            'n_gram': [1, 2, 3],
            'stop_words': [True, False],
            'case_folding': [True, False],
            'pretrained_model': ['wiki-news-300d-1M.vec', 'crawl-300d-2M.vec']
        }

        params_list = self.generate_params_list(fasttext_params)
        for params in params_list:

            # Create the fasttext specific train file
            fasttext_path = os.path.join(os.getcwd(), 'fasttext')
            fasttext_train_file_path = os.path.join(fasttext_path, 'train_data.txt')
            with open(fasttext_train_file_path, 'w', encoding='utf-8') as file:
                for index, value in self.X_train.iteritems():
                    label = '1' if bool(self.y_train[index]) is True else '0'

                    # Remove line breaks
                    text = value.replace('\r\n', ' ')

                    # Apply case_folding
                    if params['case_folding']:
                        text = text.lower()

                    # Remove stop_words
                    if params['stop_words']:
                        stop_words = set(stopwords.words('english'))
                        text = ' '.join([word for word in text.split() if word not in stop_words])

                    file.write('__label__' + label + ' ' + text + '\n')

            # Train
            absolute_vector_file_path = os.path.join(fasttext_path, 'pretrained_models', params['pretrained_model'])
            model = fasttext.train_supervised(
                input=fasttext_train_file_path,
                pretrainedVectors=absolute_vector_file_path,
                epoch=params['epochs'],
                lr=params['learning_rate'],
                dim=params['dimensionality'],
                wordNgrams=params['n_gram'],
            )

            # Test
            # NOTE: Since fasttext doesn't offer good metrics, we calculate the confusion matrix ourselves
            conf_matrix = {
                'tp': 0,
                'fp': 0,
                'fn': 0,
                'tn': 0
            }

            probabilities = []
            for index, value in self.X_test.iteritems():
                result = model.predict(value.replace('\r\n', ' '))
                prediction = True if result[0][0] == '__label__1' else False
                probabilities.append(result[1][0])
                if prediction is True:
                    if prediction == bool(self.y_test[index]):
                        conf_matrix['tp'] += 1
                    else:
                        conf_matrix['fp'] += 1
                else:
                    if prediction == bool(self.y_test[index]):
                        conf_matrix['tn'] += 1
                    else:
                        conf_matrix['fn'] += 1

            # Create result
            result = {
                'classifier': 'fasttext',
                'parameters': {
                    'vectorizer': 'fasttext',
                    'classifier': params
                },
                'confusion_matrix': conf_matrix,
                'roc': self.get_roc(probabilities)
            }
            self.result_list.append(result)

    # BERT has to be trained in an environment with a stronger GPU. This method only prepares the data for training.
    def test_bert(self):

        parameter_space = {
            'case_folding': [True, False]
        }

        params_list = self.generate_params_list(parameter_space)

        for params in params_list:

            # Prepare Data
            def clean_text(text):
                text = re.sub('(\\W)+', ' ', text)
                if params['case_folding']:
                    text = text.lower()
                return text

            data_raw = self.data[['abstract', 'label']]
            data_raw.reset_index(inplace=True, drop=True)
            data_raw['label'] = data_raw['label'].astype(int)
            data_raw.abstract.apply(clean_text)

            file_name = 'bert/prepared_data/data_raw_uncased.csv' if params['case_folding'] else 'bert/prepared_data/data_raw_cased.csv'
            data_raw.to_csv(file_name, index=False)

            df_bert = pd.DataFrame({
                'id': range(len(data_raw)),
                'label': data_raw['label'],
                'alpha': ['a']*data_raw.shape[0],
                'text': data_raw['abstract'].replace(r'[\n|\r\n]', ' ', regex=True)
            })

            df_bert_train, df_bert_test = train_test_split(df_bert, train_size=0.8)
            df_bert_train, df_bert_dev = train_test_split(df_bert_train, train_size=0.9)

            df_bert_train.to_csv('bert/prepared_data/train.tsv', sep='\t', index=False, header=False)
            df_bert_dev.to_csv('bert/prepared_data/dev.tsv', sep='\t', index=False, header=False)
            df_bert_test.to_csv('bert/prepared_data/test.tsv', sep='\t', index=False, header=False)

    # ULMFIT CLASSIFIER
    def test_ulmfit(self):
        df = self.data.drop('uid', 1)
        df['abstract'] = df['abstract'].str.replace("[^a-zA-Z]", " ")
        stop_words = stopwords.words('english')

        # Tokenization
        tokenized_docs = df['abstract'].apply(lambda x: x.split())

        # Remove stop words
        tokenized_docs = tokenized_docs.apply(lambda x: [item for item in x if item not in stop_words])

        # De-tokenization
        detokenized_docs = []
        for i in range(len(df)):
            t = ' '.join(tokenized_docs[i])
            detokenized_docs.append(t)

        df['abstract'] = detokenized_docs
        df_trn, df_val = train_test_split(df, stratify=df['label'], test_size=0.2, random_state=self.SEED)

        # Language model data
        data_lm = TextLMDataBunch.from_df(train_df=df_trn, valid_df=df_val, path="", label_cols='label', text_cols='abstract')

        # Classifier model data
        data_clas = TextClasDataBunch.from_df(path="", train_df=df_trn, valid_df=df_val, label_cols='label', text_cols='abstract', vocab=data_lm.train_ds.vocab,
                                              bs=16)

        learn = language_model_learner(data_lm, AWD_LSTM, drop_mult=0.5)

        # train the learner object with learning rate = 1e-2
        learn.fit_one_cycle(1, 1e-2)

        learn.save_encoder('ft_enc')

        learn = text_classifier_learner(data_clas, AWD_LSTM, drop_mult=0.5)
        learn.load_encoder('ft_enc')

        learn.fit_one_cycle(1, 1e-2)

        # Get predictions
        preds, targets = learn.get_preds()

        predictions = np.argmax(preds, axis=1)
        conf_matrix_df = pd.crosstab(predictions, targets)

        # Test
        # TODO: Read out confusion matrix
        conf_matrix = {
            'tp': int(conf_matrix_df[0][0]),
            'fp': int(conf_matrix_df[0][1]),
            'fn': int(conf_matrix_df[1][0]),
            'tn': int(conf_matrix_df[1][1])
        }

        # Create result
        result = {
            'classifier': 'ulmfit',
            'parameters': {
                'vectorizer': 'ulmfit',
                'classifier': 'ulmfit'
            },
            'confusion_matrix': conf_matrix,
            'roc': None
        }
        self.result_list.append(result)

    # UTILITIES
    def get_roc(self, probabilities):
        fpr, tpr, threshold = roc_curve(self.y_test, probabilities)
        roc = {
            'fpr': fpr,
            'tpr': tpr,
            'threshold': threshold
        }
        return roc

    # This function generates a list of dictionaries that each contain a different set of parameters based on different
    # values for each parameters. Example:
    # The dictionary {'n_gram': [1, 2], 'stop_words': [True, False]} becomes
    # [{'n_gram': '1', 'stop_words': True},
    #  {'n_gram': '2', 'stop_words': True},
    #  {'n_gram': '1', 'stop_words': False},
    #  {'n_gram': '2', 'stop_words': False}]
    @staticmethod
    def generate_params_list(params_dict):
        params_list = list((dict(zip(params_dict, x)) for x in itertools.product(*params_dict.values())))
        return params_list

    def seed_all(self, seed_value):
        random.seed(seed_value)  # Python
        np.random.seed(seed_value)  # cpu vars
        torch.manual_seed(seed_value)  # cpu  vars

        if torch.cuda.is_available():
            torch.cuda.manual_seed(seed_value)
            torch.cuda.manual_seed_all(seed_value)  # gpu vars
            torch.backends.cudnn.deterministic = True  # needed
            torch.backends.cudnn.benchmark = False
