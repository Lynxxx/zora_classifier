from transformers.convert_bert_original_tf_checkpoint_to_pytorch import convert_tf_checkpoint_to_pytorch

# Define which models you want to convert
MODEL_PATHS = ['bert/pretrained_models/multi_cased/']

# This snippet converts tensorflow checkpoints to pytorch checkpoints
for path in MODEL_PATHS:
    tf_checkpoint_path = path + 'bert_model.ckpt'
    bert_config_file = path + 'bert_config.json'
    pytorch_dump_path = path + 'pytorch/pytorch_model.bin'
    convert_tf_checkpoint_to_pytorch(tf_checkpoint_path=tf_checkpoint_path,
                                     bert_config_file=bert_config_file,
                                     pytorch_dump_path=pytorch_dump_path)