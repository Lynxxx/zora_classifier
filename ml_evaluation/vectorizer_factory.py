from sklearn.feature_extraction.text import CountVectorizer, HashingVectorizer, TfidfVectorizer
from sklearn.decomposition import TruncatedSVD


# Possible parameters are:
#   -data                           The trainings data to create the vocabulary.
#   -vectorizer                     The type of vectorizer (count, hashing, tf-idf)
#   -tokenizer                      The type of tokenizer (sklearn, spacy, nltk)
#   -n_gram                         The size of n_grams
#   -stop_words                     Flag that indicates whether stop_words should be used or not
#   -lemmatization                  Flag that indicates whether lemmatization should be used or not
#   -case_folding                   Flag that indicates whether case_folding should be used or not
#   -dimensionality_reduction       The type of dimensionality reduction (None, lsa)
class VectorizerFactory:
    def __init__(self, data, vectorizer='count', tokenizer='sklearn', dimensionality_reduction=None, dimensionality=200,
                 n_gram=1, stop_words=False, lemmatization=False, case_folding=False):

        # Store parameters so that we know for each vectorizer which parameters were used (needed in the evaluation)
        self.parameters = {
            'vectorizer': vectorizer,
            'tokenizer': tokenizer,
            'dimensionality_reduction': dimensionality_reduction,
            'dimensionality': dimensionality,
            'n_gram': n_gram,
            'stop_words': stop_words,
            'lemmatization': lemmatization,
            'case_folding': case_folding
        }

        tokenizer_function = None
        stop_words_param = None

        # Prepare vectorizer parameters
        if tokenizer == 'sklearn':
            n_gram_param = (n_gram, n_gram)
            if stop_words:
                stop_words_param = 'english'
        elif tokenizer == 'nltk':

            # TODO: Implement nltk tokenizer
            raise Exception('Nltk tokenizer not yet implemented.')
        elif tokenizer == 'spacy':

            # TODO: Implement spacy tokenizer
            raise Exception('Spacy tokenizer not yet implemented.')
        else:
            raise Exception('Tokenizer must be one of "sklearn", "nltk", "spacy". It was: {}'.format(tokenizer))

        # Create vectorizer
        if vectorizer == 'count':
            self.vectorizer = CountVectorizer(tokenizer=tokenizer_function, ngram_range=n_gram_param, stop_words=stop_words_param, lowercase=case_folding)
        elif vectorizer == 'hashing':
            self.vectorizer = HashingVectorizer(tokenizer=tokenizer_function, ngram_range=n_gram_param, stop_words=stop_words_param, lowercase=case_folding)
        elif vectorizer == 'tf_idf':
            self.vectorizer = TfidfVectorizer(tokenizer=tokenizer_function, ngram_range=n_gram_param, stop_words=stop_words_param, lowercase=case_folding)
        else:
            raise Exception('Vectorizer must be one of "count", "hashing", "tf_idf". It was: {}'.format(vectorizer))

        # Train vectorizer
        self.vocabulary_dtm = self.vectorizer.fit_transform(data)

        # Reduce dimensionality
        if dimensionality_reduction == 'lsa':
            self.svd = TruncatedSVD(n_components=dimensionality)
            self.vocabulary_dtm = self.svd.fit_transform(self.vocabulary_dtm)
            self.parameters['dimensionality'] = dimensionality
        else:
            self.parameters['dimensionality'] = self.vocabulary_dtm.shape[1]

    def get_vocabulary_dtm(self):
        return self.vocabulary_dtm

    def get_data_dtm(self, data):
        data_dtm = self.vectorizer.transform(data)
        if self.parameters['dimensionality_reduction'] == 'lsa':
            data_dtm = self.svd.transform(data_dtm)
        return data_dtm
