# Machine Learning Evaluation

With these scripts, it is possible to try out and evaluate a variety of different machine learning algorithms on the
ZORA classifier data. Be aware that the scripts will not work out of the box. You need to:

- Have a ZORA classifier database with annotated data
- Install the required packages (only the packages needed for the server are stored in the requireements.txt)
- Define the parameters in the \_init\_.py script as needed