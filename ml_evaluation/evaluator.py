import csv
import datetime
import os

from sklearn.metrics import auc


# This class creates the metrics based on classification results
class Evaluator:
    def __init__(self, results):
        self.results = results

        self.evaluations = []

    def evaluate_all(self):
        for result in self.results:
            self.evaluate(result)
        self.store_evaluations()

    def evaluate(self, result):
        confusion_matrix = result['confusion_matrix']
        tp = confusion_matrix['tp']
        fp = confusion_matrix['fp']
        fn = confusion_matrix['fn']
        tn = confusion_matrix['tn']

        # Calculate all metrics
        metrics = {
            'accuracy': self.get_accuracy(tp, fp, fn, tn),
            'precision': self.get_precision(tp, fp),
            'recall': self.get_recall(tp, fn),
            'f1_score': self.get_f1_score(tp, fp, fn),
            'specificity': self.get_specificity(fp, tn),
            'auc': self.get_auc(result['roc']['fpr'], result['roc']['tpr']) if result['roc'] is not None else 'Not calculated'
        }

        # Merge all evaluation data into one evaluation dictionary
        evaluation = {
            'classifier': result['classifier'],
            'vectorizer_params': result['parameters']['vectorizer'],
            'classifier_params': result['parameters']['classifier']
        }
        evaluation.update(confusion_matrix)
        evaluation.update(metrics)

        self.evaluations.append(evaluation)

    def store_evaluations(self):
        absolute_path = os.getcwd()
        timestamp = datetime.datetime.utcnow().replace(microsecond=0)
        timestamp_string = str(timestamp).replace(':', '-')
        file_path = os.path.join(absolute_path, 'evaluations', timestamp_string)

        # Create sub-folders if they don't exist
        if not os.path.exists(file_path):
            os.makedirs(file_path)

        # Write evaluations to file
        field_names = ['classifier', 'accuracy', 'precision', 'recall', 'f1_score', 'specificity', 'auc', 'vectorizer_params', 'classifier_params', 'tp', 'fp', 'fn', 'tn']
        absolute_file_path = os.path.join(file_path, 'evaluations.csv')
        with open(absolute_file_path, 'w', newline='') as file:
            writer = csv.DictWriter(file, fieldnames=field_names)
            writer.writeheader()
            for evaluation in self.evaluations:
                writer.writerow(evaluation)

    @staticmethod
    def get_accuracy(tp, fp, fn, tn):
        if (tp + fp + fn + tn) == 0:
            return 0
        return (tp + tn) / (tp + fp + fn + tn)

    @staticmethod
    def get_precision(tp, fp):
        if (tp + fp) == 0:
            return 0
        return tp / (tp + fp)

    @staticmethod
    def get_recall(tp, fn):
        if (tp + fn) == 0:
            return 0
        return tp / (tp + fn)

    @staticmethod
    def get_f1_score(tp, fp, fn):
        precision = Evaluator.get_precision(tp, fp)
        recall = Evaluator.get_recall(tp, fn)
        if (precision + recall) == 0:
            return 0
        return (2 * precision * recall) / (precision + recall)

    @staticmethod
    def get_specificity(fp, tn):
        if (fp + tn) == 0:
            return 0
        return tn / (tn + fp)

    @staticmethod
    def get_auc(fpr, tpr):
        return auc(fpr, tpr)
