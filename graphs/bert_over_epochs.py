import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick

# Load the data
data_base_cased = pd.read_csv('data/base-cased.csv').groupby(['epoch']).mean().reset_index()
epochs_base_cased = data_base_cased['epoch']
accuracy_base_cased = data_base_cased['accuracy']
max_accuracy_base_cased = np.amax(accuracy_base_cased)
max_accuracy_epoch_base_cased = data_base_cased['epoch'][accuracy_base_cased.idxmax()]
precision_base_cased = data_base_cased['precision']
max_precision_base_cased = np.amax(precision_base_cased)
max_precision_epoch_base_cased = data_base_cased['epoch'][precision_base_cased.idxmax()]
recall_base_cased = data_base_cased['recall']
max_recall_base_cased = np.amax(recall_base_cased)
max_recall_epoch_base_cased = data_base_cased['epoch'][recall_base_cased.idxmax()]
f1_score_base_cased = data_base_cased['f1_score']
max_f1_score_base_cased = np.amax(accuracy_base_cased)
max_f1_score_epoch_base_cased = data_base_cased['epoch'][f1_score_base_cased.idxmax()]

data_base_uncased = pd.read_csv('data/base-uncased.csv').groupby(['epoch']).mean().reset_index()
epochs_base_uncased = data_base_uncased['epoch']
accuracy_base_uncased = data_base_uncased['accuracy']
max_accuracy_base_uncased = np.amax(accuracy_base_uncased)
max_accuracy_epoch_base_uncased = data_base_uncased['epoch'][accuracy_base_uncased.idxmax()]
precision_base_uncased = data_base_uncased['precision']
max_precision_base_uncased = np.amax(precision_base_uncased)
max_precision_epoch_base_uncased = data_base_uncased['epoch'][precision_base_uncased.idxmax()]
recall_base_uncased = data_base_uncased['recall']
max_recall_base_uncased = np.amax(recall_base_uncased)
max_recall_epoch_base_uncased = data_base_uncased['epoch'][recall_base_uncased.idxmax()]
f1_score_base_uncased = data_base_uncased['f1_score']
max_f1_score_base_uncased = np.amax(f1_score_base_uncased)
max_f1_score_epoch_base_uncased = data_base_uncased['epoch'][f1_score_base_uncased.idxmax()]

data_large_cased = pd.read_csv('data/large-cased.csv').groupby(['epoch']).mean().reset_index()
epochs_large_cased = data_large_cased['epoch']
accuracy_large_cased = data_large_cased['accuracy']
max_accuracy_large_cased = np.amax(accuracy_large_cased)
max_accuracy_epoch_large_cased = data_large_cased['epoch'][accuracy_large_cased.idxmax()]
precision_large_cased = data_large_cased['precision']
max_precision_large_cased = np.amax(precision_large_cased)
max_precision_epoch_large_cased = data_large_cased['epoch'][precision_large_cased.idxmax()]
recall_large_cased = data_large_cased['recall']
max_recall_large_cased = np.amax(recall_large_cased)
max_recall_epoch_large_cased = data_large_cased['epoch'][recall_large_cased.idxmax()]
f1_score_large_cased = data_large_cased['f1_score']
max_f1_score_large_cased = np.amax(f1_score_large_cased)
max_f1_score_epoch_large_cased = data_large_cased['epoch'][f1_score_large_cased.idxmax()]

data_large_uncased = pd.read_csv('data/large-uncased.csv').groupby(['epoch']).mean().reset_index()
epochs_large_uncased = data_large_uncased['epoch']
accuracy_large_uncased = data_large_uncased['accuracy']
max_accuracy_large_uncased = np.amax(accuracy_large_uncased)
max_accuracy_epoch_large_uncased = data_large_uncased['epoch'][accuracy_large_uncased.idxmax()]
precision_large_uncased = data_large_uncased['precision']
max_precision_large_uncased = np.amax(precision_large_uncased)
max_precision_epoch_large_uncased = data_large_uncased['epoch'][precision_large_uncased.idxmax()]
recall_large_uncased = data_large_uncased['recall']
max_recall_large_uncased = np.amax(recall_large_uncased)
max_recall_epoch_large_uncased = data_large_uncased['epoch'][recall_large_uncased.idxmax()]
f1_score_large_uncased = data_large_uncased['f1_score']
max_f1_score_large_uncased = np.amax(f1_score_large_uncased)
max_f1_score_epoch_large_uncased = data_large_uncased['epoch'][f1_score_large_uncased.idxmax()]

fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2, figsize=(9,7))
fig.suptitle('BERT models over multiple epochs', fontsize=14)

# Subplot 1 - Accuracy
ax1.set_title('Accuracy', pad=10)
ax1.plot(epochs_base_cased, accuracy_base_cased, color='royalblue', label='base-cased', marker='.', markersize=10, markevery=[max_accuracy_epoch_base_cased - 1])
ax1.axhline(max_accuracy_base_cased, linestyle='--', linewidth=0.5, color='royalblue')
ax1.axvline(max_accuracy_epoch_base_cased, linestyle='--', linewidth=0.5, color='royalblue')

ax1.plot(epochs_base_uncased, accuracy_base_uncased, color='crimson', label='base-uncased', marker='.', markersize=10, markevery=[max_accuracy_epoch_base_uncased - 1])
ax1.axhline(max_accuracy_base_uncased, linestyle='--', linewidth=0.5, color='crimson')
ax1.axvline(max_accuracy_epoch_base_uncased, linestyle='--', linewidth=0.5, color='crimson')

ax1.plot(epochs_large_cased, accuracy_large_cased, color='orange', label='large-cased', marker='.', markersize=10, markevery=[max_accuracy_epoch_large_cased - 1])
ax1.axhline(max_accuracy_large_cased, linestyle='--', linewidth=0.5, color='orange')
ax1.axvline(max_accuracy_epoch_large_cased, linestyle='--', linewidth=0.5, color='orange')

ax1.plot(epochs_large_uncased, accuracy_large_uncased, color='seagreen', label='large-uncased', marker='.', markersize=10, markevery=[max_accuracy_epoch_large_uncased - 1])
ax1.axhline(max_accuracy_large_uncased, linestyle='--', linewidth=0.5, color='seagreen')
ax1.axvline(max_accuracy_epoch_large_uncased, linestyle='--', linewidth=0.5, color='seagreen')

ax1.spines['top'].set_visible(False)
ax1.spines['right'].set_visible(False)
ax1.tick_params(axis='both', labelsize=9)
ax1.set_xlabel('Epoch', fontsize=9)
ax1.xaxis.set_label_coords(0.95, -0.13)
ax1.yaxis.set_major_formatter(mtick.PercentFormatter(1.0))
ax1.set_xlim(0, 21)
ax1.set_ylim(0.55, 1.01)

# Subplot 2 - Precision
ax2.set_title('Precision', pad=10)
ax2.plot(epochs_base_cased, precision_base_cased, color='royalblue', label='base-cased', marker='.', markersize=10, markevery=[max_precision_epoch_base_cased - 1])
ax2.axhline(max_precision_base_cased, linestyle='--', linewidth=0.5, color='royalblue')
ax2.axvline(max_precision_epoch_base_cased, linestyle='--', linewidth=0.5, color='royalblue')

ax2.plot(epochs_base_uncased, precision_base_uncased, color='crimson', label='base-uncased', marker='.', markersize=10, markevery=[max_precision_epoch_base_uncased - 1])
ax2.axhline(max_precision_base_uncased, linestyle='--', linewidth=0.5, color='crimson')
ax2.axvline(max_precision_epoch_base_uncased, linestyle='--', linewidth=0.5, color='crimson')

ax2.plot(epochs_large_cased, precision_large_cased, color='orange', label='large-cased', marker='.', markersize=10, markevery=[max_precision_epoch_large_cased - 1])
ax2.axhline(max_precision_large_cased, linestyle='--', linewidth=0.5, color='orange')
ax2.axvline(max_precision_epoch_large_cased, linestyle='--', linewidth=0.5, color='orange')

ax2.plot(epochs_large_uncased, precision_large_uncased, color='seagreen', label='large-uncased', marker='.', markersize=10, markevery=[max_precision_epoch_large_uncased - 1])
ax2.axhline(max_precision_large_uncased, linestyle='--', linewidth=0.5, color='seagreen')
ax2.axvline(max_precision_epoch_large_uncased, linestyle='--', linewidth=0.5, color='seagreen')

ax2.spines['top'].set_visible(False)
ax2.spines['right'].set_visible(False)
ax2.tick_params(axis='both', labelsize=9)
ax2.set_xlabel('Epoch', fontsize=9)
ax2.xaxis.set_label_coords(0.95, -0.13)
ax2.yaxis.set_major_formatter(mtick.PercentFormatter(1.0))
ax2.set_xlim(0, 21)
ax2.set_ylim(0.55, 1.01)

# Subplot 3 - Recall
ax3.set_title('Recall', pad=10)
ax3.plot(epochs_base_cased, recall_base_cased, color='royalblue', label='base-cased', marker='.', markersize=14, markevery=[max_recall_epoch_base_cased - 1])
ax3.axhline(max_recall_base_cased, linestyle='--', linewidth=0.5, color='royalblue')
ax3.axvline(max_recall_epoch_base_cased, linestyle='--', linewidth=0.5, color='royalblue')

ax3.plot(epochs_base_uncased, recall_base_uncased, color='crimson', label='base-uncased', marker='.', markersize=10, markevery=[max_recall_epoch_base_uncased - 1])
ax3.axhline(max_recall_base_uncased, linestyle='--', linewidth=0.5, color='crimson')
ax3.axvline(max_recall_epoch_base_uncased, linestyle='--', linewidth=0.5, color='crimson')

ax3.plot(epochs_large_cased, recall_large_cased, color='orange', label='large-cased', marker='.', markersize=8, markevery=[max_recall_epoch_large_cased - 1])
ax3.axhline(max_recall_large_cased, linestyle='--', linewidth=0.5, color='orange')
ax3.axvline(max_recall_epoch_large_cased, linestyle='--', linewidth=0.5, color='orange')

ax3.plot(epochs_large_uncased, recall_large_uncased, color='seagreen', label='large-uncased', marker='.', markersize=10, markevery=[max_recall_epoch_large_uncased - 1])
ax3.axhline(max_recall_large_uncased, linestyle='--', linewidth=0.5, color='seagreen')
ax3.axvline(max_recall_epoch_large_uncased, linestyle='--', linewidth=0.5, color='seagreen')

ax3.spines['top'].set_visible(False)
ax3.spines['right'].set_visible(False)
ax3.tick_params(axis='both', labelsize=9)
ax3.set_xlabel('Epoch', fontsize=9)
ax3.xaxis.set_label_coords(0.95, -0.13)
ax3.yaxis.set_major_formatter(mtick.PercentFormatter(1.0))
ax3.set_xlim(0, 21)
ax3.set_ylim(0.55, 1.01)

# Subplot 4 - F1 Score
ax4.set_title('F1 Score', pad=10)
ax4.plot(epochs_base_cased, f1_score_base_cased, color='royalblue', label='base-cased', marker='.', markersize=10, markevery=[max_f1_score_epoch_base_cased - 1])
ax4.axhline(max_f1_score_base_cased, linestyle='--', linewidth=0.5, color='royalblue')
ax4.axvline(max_f1_score_epoch_base_cased, linestyle='--', linewidth=0.5, color='royalblue')

ax4.plot(epochs_base_uncased, f1_score_base_uncased, color='crimson', label='base-uncased', marker='.', markersize=10, markevery=[max_f1_score_epoch_base_uncased - 1])
ax4.axhline(max_f1_score_base_uncased, linestyle='--', linewidth=0.5, color='crimson')
ax4.axvline(max_f1_score_epoch_base_uncased, linestyle='--', linewidth=0.5, color='crimson')

ax4.plot(epochs_large_cased, f1_score_large_cased, color='orange', label='large-cased', marker='.', markersize=10, markevery=[max_f1_score_epoch_large_cased - 1])
ax4.axhline(max_f1_score_large_cased, linestyle='--', linewidth=0.5, color='orange')
ax4.axvline(max_f1_score_epoch_large_cased, linestyle='--', linewidth=0.5, color='orange')

ax4.plot(epochs_large_uncased, f1_score_large_uncased, color='seagreen', label='large-uncased', marker='.', markersize=10, markevery=[max_f1_score_epoch_large_uncased - 1])
ax4.axhline(max_f1_score_large_uncased, linestyle='--', linewidth=0.5, color='seagreen')
ax4.axvline(max_f1_score_epoch_large_uncased, linestyle='--', linewidth=0.5, color='seagreen')

ax4.spines['top'].set_visible(False)
ax4.spines['right'].set_visible(False)
ax4.tick_params(axis='both', labelsize=9)
ax4.set_xlabel('Epoch', fontsize=9)
ax4.xaxis.set_label_coords(0.95, -0.13)
ax4.yaxis.set_major_formatter(mtick.PercentFormatter(1.0))
ax4.set_xlim(0, 21)
ax4.set_ylim(0.55, 1.01)

# Legend
handles, labels = ax1.get_legend_handles_labels()
lgd = fig.legend(handles=handles, labels=labels, loc='lower center', ncol=4, fancybox=True, shadow=True)

# Positioning
fig.tight_layout(pad=2.5)
fig.subplots_adjust(top=0.90)
fig.subplots_adjust(bottom=0.12)

fig.savefig('output/bert_over_epochs.png')
fig.show()
