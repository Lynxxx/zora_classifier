import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# Load the data
data = pd.read_csv('data/metrics_overview_bert.csv')
classifiers = data['classifier']
accuracy = data['accuracy']
precision = data['precision']
recall = data['recall']
f1_score = data['f1_score']

pos = np.arange(len(classifiers))
plt.grid(b=True, which='major', axis='y')
plt.minorticks_on()
plt.bar(pos - 0.3, accuracy, width=0.2, color='royalblue', align='center', label='Accuracy')
plt.bar(pos - 0.1, precision, width=0.2, color='crimson', align='center', label='Precision')
plt.bar(pos + 0.1, recall, width=0.2, color='orange', align='center', label='Recall')
plt.bar(pos + 0.3, f1_score, width=0.2, color='seagreen', align='center', label='F1 Score')
plt.xticks(pos, classifiers, rotation=90)
plt.tick_params(axis='x', which='both', bottom=False, top=False)
plt.ylim((0.65, 1))
plt.legend()
plt.tight_layout()
plt.savefig('output/metrics_overview_bert.png')
plt.show()
